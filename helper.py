import random
import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread as gif_imread

def visualize(**images):
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        # plt.xticks([])
        # plt.yticks([])
        plt.title(' '.join(name.split('_')).title())
        plt.imshow(image)
    plt.show()

def show(index, dataset, transforms=None):

    image = dataset[index]['image']
    mask = dataset[index]['mask']

    if transforms is not None:
        temp = transforms(image=image, mask=mask)
        image = temp["image"]
        mask = temp["mask"]

    visualize(image=image, mask=mask)

def show_random(dataset, transforms=None):
    length = len(dataset)
    index = random.randint(0, length - 1)
    show(index, dataset, transforms)